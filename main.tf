module "new-vpc" {
  source = "./modules/vpc"
  prefix = var.prefix
}

module "eks" {
  prefix = var.prefix
  source = "./modules/eks"
  cluster_name = var.cluster_name

  vpc_id = module.new-vpc.vpc_id
  subnet_ids   = module.new-vpc.subnet_ids
  private_subnet_ids = module.new-vpc.private_subnet_ids
  public_subnet_ids = module.new-vpc.public_subnet_ids

  retention_days = var.retention_days

  desired_size = var.desired_size
  max_size     = var.max_size
  min_size     = var.min_size
}